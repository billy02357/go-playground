package main

import (
	"bytes"
	"fmt"
	"os"
)

func reverse(s string) string {
	n := len(s)
	runes := make([]rune, n)
	for _, rune := range s {
		n--
		runes[n] = rune
	}
	return string(runes[n:])
}

func comma(s string) string {
	var buf bytes.Buffer
	r := reverse(s)
	b := []byte(r)

	n := len(s)
	if n <= 3 {
		buf.WriteString(string(b))
		return reverse(buf.String())
	}
	i := 0
	for n > i {
		buf.WriteString(string(b[i : i+3]))
		if i+3 >= n {
			return reverse(buf.String())
		}
		buf.WriteString(",")
		i += 3
	}
	return reverse(buf.String())
}

func main() {
	fmt.Printf("%s\n", comma(os.Args[1]))
}
