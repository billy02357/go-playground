package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

func exitIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func printHelp() {
	fmt.Fprintln(os.Stderr, "usage: sphere [radius...]")
	fmt.Fprintln(os.Stderr, "\tOutput will be the volume, surface and circumference of the sphere with radius [radius]")
}

func main() {
	if os.Args[1] == "-h" || os.Args[1] == "help" || os.Args[1] == "--help" {
		printHelp()
		return
	}

	for i := 1; i < len(os.Args); i++ {
		r, err := strconv.ParseFloat(os.Args[i], 64)
		exitIfError(err)

		vol := 4.0 / 3.0 * math.Pi * r * r * r
		surface := 4.0 * math.Pi * r * r
		circum := 2.0 * math.Pi * r

		fmt.Printf("Volume of radius %v: %v\n", r, vol)
		fmt.Printf("Surface of radius %v: %v\n", r, surface)
		fmt.Printf("Circumference of radius %v: %v\n", r, circum)
	}

}
