// Lissajous generates GIF animations of random Lissajous figures.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var green = color.RGBA{0x00, 0xFF, 0x00, 0xFF}
var black = color.RGBA{0x00, 0x00, 0x00, 0xFF}
var blue = color.RGBA{0x00, 0x00, 0xFF, 0xFF}
var yellow = color.RGBA{0xFF, 0xFF, 0x00, 0xFF}

var palette = []color.Color{black, green, blue, yellow}

const (
	blackIndex  = 0
	greenIndex  = 1
	blueIndex   = 2
	yellowIndex = 3
)

func main() {
	lissajous(os.Stdout, os.Args[1])
}

func lissajous(out io.Writer, arg string) {
	const (
		cycles  = 5
		res     = 0.001
		size    = 100
		nframes = 64
		delay   = 8
	)
	freq := rand.Float64() * 3.0
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			if arg == "green" {
				img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), greenIndex)
			} else if arg == "yellow" {
				img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), yellowIndex)
			} else if arg == "blue" {
				img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), blueIndex)
			} else {
				fmt.Fprintf(os.Stderr, "lissajous3: Invalid color \"%s\"\n", arg)
				os.Exit(1)
			}
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim)
}
