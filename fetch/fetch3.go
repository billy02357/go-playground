// fetch3 adds prefix https:// automatically when not found
package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {
	for _, url := range os.Args[1:] {
		if strings.HasPrefix(url, "https://") || strings.HasPrefix(url, "http://") {
			fetch(url)
		} else {
			url = "https://" + url
			fetch(url)
		}
	}
}

func fetch(url string) {
	resp, errUrl := http.Get(url)
	if errUrl != nil {
		fmt.Fprintf(os.Stderr, "fetch: %v\n", errUrl)
		os.Exit(1)
	}
	b, errBody := io.Copy(os.Stdout, resp.Body)
	resp.Body.Close()
	if errBody != nil {
		fmt.Fprintf(os.Stderr, "fetch %v\n", errBody)
		os.Exit(1)
	}
	fmt.Printf("%s\n", b)
}
