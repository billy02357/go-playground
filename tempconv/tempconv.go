package main

import "fmt"

type Celsius float64
type Fahrenheit float64
type Kelvin float64

// Celsius constants
const (
	AbsoluteZeroC Celsius = -273.15
	FreezingC     Celsius = 0
	BoilingC      Celsius = 100
)

// Fahrenheit constants
const (
	AbsoluteZeroF Fahrenheit = -459.67
	FreezingF     Fahrenheit = 32
	BoilingF      Fahrenheit = 212
)

// Kelvin constants
const (
	AbsoluteZeroK Kelvin = 0
	FreezingK     Kelvin = 273.15
	BoilingK      Kelvin = 373.15
)

// Celsius
func CtoF(c Celsius) Fahrenheit {
	return Fahrenheit(c*9/5 + 32)
}

func CtoK(c Celsius) Kelvin {
	return Kelvin(c + 273.15)
}

// Fahrenheit
func FtoC(f Fahrenheit) Celsius {
	return Celsius((f - 32) * 5 / 9)
}

func FtoK(f Fahrenheit) Kelvin {
	return Kelvin((f-32)*5/9 + 273.15)
}

// Kelvin
func KtoC(k Kelvin) Celsius {
	return Celsius(k - 273.15)
}

func KtoF(k Kelvin) Fahrenheit {
	return Fahrenheit((k-273.15)*9/5 + 32)
}

func (c Celsius) StringC() string {
	return fmt.Sprintf("%g degrees C", c)
}
